import { Layout } from "@Components/layout"
import { Spinner } from "@Components/spinner";
import { _IActions } from "@Redux/storeParts";
import { getFetching, getSingleUser } from "@Selectors";
import { _IUser } from "@Utils/types";
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import styled from "styled-components";

const RenderIfExist = ( value: any, Component: JSX.Element ) => {
  return Boolean(value) ? Component : null;
}

export const User = () => {

  const dispatch = useDispatch();
  const user = useSelector(getSingleUser);
  const isFetching = useSelector(getFetching);
  const { login: loginInParams }: { login: _IUser['login'] } = useParams();

  const isUserExists = Object.keys(user).length !== 0;

  useEffect(() => {
    if (!isUserExists || loginInParams !== (user as _IUser).login) {
      console.log('dispatch');
      dispatch<_IActions['fetchUser']>({ type: 'fetchUser', value: loginInParams });
    }
  }, []);

  if (isFetching) {
    return (
      <Layout>
        <div className='container'>
          <Spinner />
        </div>
      </Layout>
    )
  }

  if (!isUserExists) {
    return (
      <Layout>
        <div className='container'>
          User is undefined
        </div>
      </Layout>
    )
  }

  const {
    login,
    name,
    followers,
    following,
    created_at,
    company,
    email,
    location,
    blog,
    bio,
    avatar_url
  } = user as _IUser;



  return (
    <Layout>
      <div className='container'>
        { RenderIfExist( avatar_url, <Image src={avatar_url as unknown as string} alt={name} /> ) }
        { RenderIfExist( login,       <p>login: { login }</p> ) }
        { RenderIfExist( name,        <p>name: { name }</p> ) }
        { RenderIfExist( followers,   <p>followers: { followers }</p> ) }
        { RenderIfExist( following,   <p>following: { following }</p> ) }
        { RenderIfExist( created_at,  <p>created_at: { created_at }</p> ) }
        { RenderIfExist( company,     <p>company: { company }</p> ) }
        { RenderIfExist( email,       <p>email: { email }</p> ) }
        { RenderIfExist( location,    <p>location: { location }</p> ) }
        { RenderIfExist( blog,        <p>blog: { blog }</p> ) }
        { RenderIfExist( bio,         <p>bio: { bio }</p> ) }
      </div>
    </Layout>
  )
}

const Image = styled.img`
  width: 200px;
  height: 200px;
`;
