import { Layout } from "@Components/layout"
import { Paginage } from "@Components/paginate";
import { UserList } from "@Components/userList";
import { _IActions } from "@Redux/storeParts";
import { getUsersPerPage, getPageNumber } from "@Selectors";
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import styled from "styled-components";

export const Users = ( ) => {

  const dispatch = useDispatch();
  const pageNumber = useSelector(getPageNumber);
  const usersPerPage = useSelector(getUsersPerPage);

  useEffect(()=>{
    dispatch<_IActions['fetchUsers']>({type:'fetchUsers', value: null});
  }, []);

  const handleUsersPerPageChange = ( e:React.ChangeEvent<HTMLInputElement>  )=>{
    const value = Number(e.target.value);
    if( value < 1 )
      return;

    if( value > 100 )
      return;

    dispatch<_IActions['setUsersPerPage']>({type: 'setUsersPerPage', value });
  }

  return (
    <Layout>
      <div className='container'>
        <Label>
          How many users should be shown? ( {pageNumber * usersPerPage} - {pageNumber * usersPerPage + usersPerPage} )
          <Input value={usersPerPage} type='number' onChange={handleUsersPerPageChange} />
        </Label>
        <UserList/>
        <StyledPaginage />
      </div>
    </Layout>
  )
}

const Label = styled.label`
  margin-bottom: 30px;
  display: block;
`;

const Input = styled.input`
  margin-left: 10px;
`;

const StyledPaginage = styled(Paginage)`
  margin-top: 30px;
`;
