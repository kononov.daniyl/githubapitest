import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom"
import { Home } from './pages/home';
import { User } from './pages/user';
import { Users } from './pages/users';

export const useRoutes = () => {

  return (
    <Switch>
      <Route path='/' exact>
        <Home/>
      </Route>
      <Route path='/users' exact>
        <Users/>
      </Route>
      <Route path='/users/:login' exact>
        <User/>
      </Route>
      <Redirect to='/' />
    </Switch>
  )
}