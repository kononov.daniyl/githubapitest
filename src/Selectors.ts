import { _IStore } from "@Redux/storeParts"

export const getUsersPerPage = ( s: _IStore ) => s.usersPerPage;
export const getPageNumber = ( s: _IStore ) => s.pageNumber;
export const getAllUsers = ( s:_IStore ) => s.users;
export const getFetching = ( s:_IStore ) => s.isFetching;
export const getSingleUser = ( s:_IStore ) => s.singleUser;
