import React from 'react';
import ReactDom from 'react-dom';
import { App } from './App';
import { GeneralStyles } from './generalStyles';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux';

ReactDom.render(
  <>
    <GeneralStyles />
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </>,
  document.getElementById('app')
);
