import { ISODate, NodeId } from "./brands";

export interface _ITableUser {
  login: string;
  id: number;
  node_id: NodeId;
  avatar_url: URL;
  gravatar_id: string;
  url: URL;
  html_url: URL;
  followers_url: URL;
  following_url: URL;
  gists_url: URL;
  starred_url: URL;
  subscriptions_url: URL;
  organizations_url: URL;
  repos_url: URL;
  events_url: URL;
  received_events_url: URL;
  type: 'User'
  site_admin: boolean;
}

export interface _IUser {
  login: string;
  id: number;
  node_id: NodeId;
  avatar_url: URL;
  gravatar_id: string;
  url: URL;
  html_url: URL;
  followers_url: URL;
  following_url: URL;
  gists_url: URL;
  starred_url: URL;
  subscriptions_url: URL;
  organizations_url: URL;
  repos_url: URL;
  events_url: URL;
  received_events_url: URL;
  type: "User"
  site_admin: boolean;
  name: string;
  company: string
  blog: URL;
  location: string;
  email: string
  hireable: string
  bio: string
  twitter_username: string
  public_repos: number;
  public_gists: number;
  followers: number;
  following: number;
  created_at: ISODate;
  updated_at: ISODate;
}