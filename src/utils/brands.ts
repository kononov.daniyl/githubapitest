type Brand<K, T> = K & { __brand: T }
type BrandedString<T> = Brand<string, T>;

/** @example MDQ6VXNlcjE= */
export type NodeId = BrandedString<'node_id'>

/** @example 2020-09-22T15:50:44Z */
export type ISODate = BrandedString<'date'>;