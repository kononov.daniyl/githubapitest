export default {
  colors: {
    /**#E8EDDF ( alabaster ) */
    interactive: '#E8EDDF',
    /**#F5CB5C ( maize crayola ) */
    interactiveHover: '#F5CB5C',
    /**#CC8963 ( brown ) */
    accent: '#CC8963',
    /**#242423 ( eerie black ) */
    text: '#242423',
    /**#333533 ( jet ) */
    darkBg: '#333533',
    /**#CFDBD5 ( timberwolf ) */
    interactiveBg: '#CFDBD5',
    /**#d32f2f ( red? ) */
    danger: '#d32f2f',
    /**#ff9800 ( yellow? ) */
    warning: '#ff9800',
    /**#43a047 ( green? ) */
    success: '#43a047',
  },
  gradients: {

  },
  fonts: {
    main: 'Montserrat',
    second: 'Arial'
  },
  fontWeight: {
    regular: 400,
    medium: 500,
    semiBold: 600,
  },
  transition: '.2s ease',
  lineHeight: '1.5em',
  regularFontSize: '18px',
  titles: {
    h1: {
      fontSize: '36px',
      lineHeight: '2em'
    },
    h2: {
      fontSize: '24px',
      lineHeight: '1.5em'
    }
  },
  sizes: {
    w1: 1919,
    w2: 1439,
    w3: 1023,
    w4: 639,
  }
} as const;
