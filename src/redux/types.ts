import { _IActionTypes } from './storeParts';

export interface _IGenericAction<T extends keyof _IActionTypes, V> {
  type: T;
  value: V;
}