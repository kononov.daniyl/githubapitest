import { _ITableUser } from '@Utils/types';
import { AllActions, _IStore } from '.';
import { _IGenericAction } from '../types';

declare module './index' {
  interface _IStore {
    isFetching: boolean;
  }

  interface _IActionTypes {
    setIsFetching: 'setIsFetching';
  }

  interface _IActions {
    setIsFetching: _IGenericAction<_IActionTypes['setIsFetching'], boolean>;
  }
}

export const isFetching = function isFetching( state: _IStore['isFetching'] = false, action: AllActions ) {
  switch( action.type ) {
    case 'setIsFetching':
      return action.value;
    default:
      return state;
  }
};
