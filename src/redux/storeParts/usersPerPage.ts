import { AllActions, _IStore } from '.';
import { _IGenericAction } from '../types';

declare module './index' {
  interface _IStore {
    usersPerPage: number;
  }

  interface _IActionTypes {
    setUsersPerPage: 'setUsersPerPage';
  }

  interface _IActions {
    setUsersPerPage: _IGenericAction<_IActionTypes['setUsersPerPage'], number>;
  }
}

export const usersPerPage = function usersPerPage( state: _IStore['usersPerPage'] = 10, action: AllActions ) {
  switch( action.type ) {
    case 'setUsersPerPage':
      return action.value;
    default:
      return state;
  }
};
