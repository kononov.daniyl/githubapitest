import { _IGenericAction } from '../types';
import { _IStore, AllActions, _IActions } from '.';
import { select, call, put } from 'redux-saga/effects';
import { _ITableUser } from '@Utils/types';
import { getPageNumber, getUsersPerPage } from '@Selectors';

declare module './index' {
  interface _IStore {
    users: _ITableUser[];
  }

  interface _IActionTypes {
    setUsers: 'setUsers';
    fetchUsers: 'fetchUsers';
  }

  interface _IActions {
    setUsers: _IGenericAction<_IActionTypes['setUsers'], _ITableUser[]>;
    fetchUsers: _IGenericAction<_IActionTypes['fetchUsers'], null>;
  }
}

export const users = function users(state: _IStore['users'] = [], action: AllActions) {

  switch (action.type) {
    case 'setUsers':
      return action.value;
    default:
      return state;
  }
};

export function* fetchUsers() {
  const usersPerPage: _IStore['usersPerPage'] = yield select(getUsersPerPage);
  const pageNumber: _IStore['pageNumber'] = yield select(getPageNumber);

  yield put<_IActions['setIsFetching']>({ type: 'setIsFetching', value: true });

  const url = `https://api.github.com/users?per_page=${usersPerPage}&since=${pageNumber}`;

  try {
    const data = yield call(() => fetch(url, {
      method: "GET",
      headers: {
        Accept: 'application/vnd.github.v3+json'
      }
    }));

    const users: _ITableUser[] = yield data.json();

    yield put<_IActions['setUsers']>({ type: 'setUsers', value: users });
    yield put<_IActions['setIsFetching']>({ type: 'setIsFetching', value: false });
  } catch (e) {
    yield put<_IActions['setUsers']>({ type: 'setUsers', value: [] });
    yield put<_IActions['setIsFetching']>({ type: 'setIsFetching', value: false });
    console.log('e', e);
  }
}
