import { combineReducers } from 'redux';
import {users} from './users';
import {usersPerPage} from './usersPerPage';
import {pageNumber} from './pageNumber';
import {singleUser} from './singleUser';
import {isFetching} from './isFetching';

export interface _IStore {}
export interface _IActionTypes {}
export interface _IActions {}

export type AllActions = _IActions[keyof _IActions];

export const rootReducer = combineReducers<_IStore, AllActions>( {
  users,
  usersPerPage,
  pageNumber,
  singleUser,
  isFetching,
});

