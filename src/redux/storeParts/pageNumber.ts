import { AllActions, _IStore } from '.';
import { _IGenericAction } from '../types';

declare module './index' {
  interface _IStore {
    pageNumber: number;
  }

  interface _IActionTypes {
    setPageNumber: 'setPageNumber';
  }

  interface _IActions {
    setPageNumber: _IGenericAction<_IActionTypes['setPageNumber'], number>;
  }
}

export const pageNumber = function pageNumber( state: _IStore['pageNumber'] = 0, action: AllActions ) {
  switch( action.type ) {
    case 'setPageNumber':
      return action.value;
    default:
      return state;
  }
};
