import { _IUser } from '@Utils/types';
import { call, put } from 'redux-saga/effects';
import { AllActions, _IActions, _IStore } from '.';
import { _IGenericAction } from '../types';

declare module './index' {
  interface _IStore {
    singleUser: _IUser | {};
  }

  interface _IActionTypes {
    setSingleUser: 'setSingleUser';
    fetchUser: 'fetchUser';
  }

  interface _IActions {
    setSingleUser: _IGenericAction<_IActionTypes['setSingleUser'], _IUser>;
    fetchUser: _IGenericAction<_IActionTypes['fetchUser'], _IUser['login']>;
  }
}

export const singleUser = function singleUser( state: _IStore['singleUser'] = {}, action: AllActions ) {
  switch( action.type ) {
    case 'setSingleUser':
      return action.value;
    default:
      return state;
  }
};

export function* fetchUser(action: _IActions['fetchUser']) {
  yield put<_IActions['setIsFetching']>({ type: 'setIsFetching', value: true });

  try {
    const data = yield call(() => fetch(`https://api.github.com/users/${action.value}`, {
      method: "GET",
      headers: {
        Accept: 'application/vnd.github.v3+json'
      }
    }));

    const user: _IUser = yield data.json();

    yield put<_IActions['setSingleUser']>({ type: 'setSingleUser', value: user });
    yield put<_IActions['setIsFetching']>({ type: 'setIsFetching', value: false });
  } catch (e) {
    console.log('e', e);
  }
}
