import { createStore, applyMiddleware  } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { _IStore, AllActions, rootReducer } from './storeParts';
import { composeWithDevTools } from 'redux-devtools-extension';
import {rootSata} from './sagas';

const sagaMiddleware = createSagaMiddleware()

export const store = createStore<_IStore, AllActions, {}, {}>(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(sagaMiddleware)
  ),
);

sagaMiddleware.run(rootSata)
