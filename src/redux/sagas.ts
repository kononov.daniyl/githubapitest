import { takeEvery, all } from 'redux-saga/effects'
import { _IActions } from './storeParts';
import { fetchUser } from './storeParts/singleUser';
import { fetchUsers } from './storeParts/users';

function* rootSata() {
  yield all([
    takeEvery<_IActions['setUsersPerPage']>('setUsersPerPage', fetchUsers),
    takeEvery<_IActions['setPageNumber']>('setPageNumber', fetchUsers),
    takeEvery<_IActions['fetchUsers']>('fetchUsers', fetchUsers),
    takeEvery<_IActions['fetchUser']>('fetchUser', fetchUser),
  ]);
}

export {rootSata};