
import { Button } from "@Components/button";
import { _IActions } from "@Redux/storeParts";
import Theme from "@Theme";
import { _ITableUser } from "@Utils/types";
import React from "react";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";

export const UserItem = (p: _ITableUser) => {
  const {
    login,
    avatar_url,
    html_url
  } = p;

  const history = useHistory();

  const userUrl = `/users/${login}`;

  const handleOpenUser = () => {
    history.push(userUrl);
  }

  return (
    <_UserItem>
      <Link to={userUrl}>
        <Image src={avatar_url as unknown as string} alt={login} />
      </Link>
      <Info>
        <Texts>
          <Link to={userUrl}>
            <Login>
              {login}
            </Login>
          </Link>
          <ProfileLink href={html_url as unknown as string} target='_blank' rel='noopener noreferrer'>
            Github link
          </ProfileLink>
        </Texts>
        <StyledButton onClick={handleOpenUser}>
          View profile
        </StyledButton>
      </Info>
    </_UserItem>
  )
}

const _UserItem = styled.div`
  display: flex;
  padding: 10px;
  border: 1px solid ${Theme.colors.darkBg};
  border-radius: 3px;
`;

const Image = styled.img`
  width: 100px;
  height: 100px;
  flex-shrink: 0;
`;

const Info = styled.div`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: baseline;
  flex-grow: 1;
`;

const Login = styled.p`
  font-size: 20px;
  font-weight: ${Theme.fontWeight.medium};
`;

const ProfileLink = styled.a`
  border-bottom: 1px solid ${Theme.colors.darkBg};
`;

const Texts = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const StyledButton = styled(Button)`
  color: ${Theme.colors.darkBg};
  margin-top: auto;
  padding: 0;
  margin-left: auto;

  &:hover{
    color: ${Theme.colors.accent};
  }
`;
