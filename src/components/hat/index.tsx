import React from 'react';
import styled from 'styled-components';
import Theme from '@Theme';
import { Link } from 'react-router-dom';
import { Button } from '@Components/button';

export const Hat = () => {

  const nav = [{
    text: 'Home',
    link: '/',
  }, {
    text: 'Users',
    link: '/users'
  }];

  return (
    <Wrap>
      <div className="container">
        <Inner>
          <Nav>
            {
              nav.map(({ text, link }) => (
                <Link key={text} to={link}>
                  <Button>
                    {text}
                  </Button>
                </Link>
              ))
            }
          </Nav>
        </Inner>
      </div>
    </Wrap>
  )
};

const Wrap = styled.div`
  background: ${Theme.colors.darkBg};
  padding: 20px 0;
  margin-bottom: 50px;
`;

const Inner = styled.div`
  display: flex;
  align-items: center;
`;

const Nav = styled.nav`
  display: flex;
  align-items: center;

  a{
    &:not(:last-of-type){
      margin-right: 10px;
    }
  }
`;
