import React from "react";
import styled from "styled-components";
import Theme from "@Theme";

interface _IButton {
  onClick?(): void;
  children: string;
  className?: string;
  isDisabled?: boolean;
}

export const Button = (p: _IButton) => {
  const {
    onClick = ()=>{},
    children,
    className,
    isDisabled = false,
  } = p;

  return (
    <Btn
      className={className}
      onClick={onClick}
      disabled={isDisabled}
    >
      {children}
    </Btn>
  );
}

const Btn = styled.button<{disabled: boolean}>`
  background: none;
  color: ${Theme.colors.interactive};
  transition: ${Theme.transition};
  border: none;
  cursor: ${({disabled})=> disabled ? 'no-drop' : 'pointer'};

  &:hover{
    color: ${Theme.colors.interactiveHover};
    transform: translateY(-2px);
  }

  &:active{
    transform: translateY(0px);
    color: ${Theme.colors.interactive};
  }

  &[disabled]{
    opacity: .5;
  }
`;