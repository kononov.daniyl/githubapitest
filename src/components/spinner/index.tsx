import Theme from "@Theme";
import React from "react";
import styled, { keyframes } from "styled-components"


export const Spinner = () => <_Spinner />;

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const _Spinner = styled.div`
  width: 100px;
  height: 100px;
  border: 10px solid ${Theme.colors.darkBg};
  border-top-color: ${Theme.colors.accent};
  border-radius: 50%;
  animation: ${rotate} 1s linear infinite;
`;

