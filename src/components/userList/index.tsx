import { Spinner } from "@Components/spinner";
import { UserItem } from "@Components/userItem";
import { getAllUsers, getFetching } from "@Selectors";
import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";


export const UserList = () => {

  const users = useSelector(getAllUsers);
  const isFetching = useSelector(getFetching);

  if ( isFetching ){
    return(
      <Spinner />
    )
  }

  if ( users.length === 0 ){
    return(
      <EmptyList>List is empty</EmptyList>
    )
  }

  return (
    <_UserList>
      {
        users.map((user) => (
          <UserItem {...user} key={user.id} />
        ))
      }
    </_UserList>
  )
}

const _UserList = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 5px;
`;

const EmptyList = styled.div`

`;
