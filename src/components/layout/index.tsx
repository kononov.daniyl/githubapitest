import React from 'react';
import { Hat } from "@Components/hat"

interface _ILayput {
  children?: JSX.Element | JSX.Element[] | string | number;
}

export const Layout = (p: _ILayput) => {
  const {
    children
  } = p;

  return (
    <>
      <Hat />
      {children}
    </>
  )
}