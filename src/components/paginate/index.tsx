import { _IActions } from "@Redux/storeParts";
import { getPageNumber, getUsersPerPage } from "@Selectors";
import Theme from "@Theme";
import React from "react"
import ReactPaginate from "react-paginate";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

interface _Props {
  className?: string;
}

export const Paginage = (p:_Props) => {
  const { className } = p;

  const dispatch = useDispatch();
  const pageNumber = useSelector(getPageNumber);
  const usersPerPage = useSelector(getUsersPerPage);
  const usersLimit = 100;

  const handlePageClick = ({selected}:{selected: number}) => {
    dispatch<_IActions['setPageNumber']>({type:'setPageNumber', value: selected});
  };

  return (
    <_Paginate>
      <ReactPaginate
        forcePage={pageNumber}
        previousLabel={<NextPrevLabel>prev</NextPrevLabel>}
        nextLabel={<NextPrevLabel>next</NextPrevLabel>}
        breakLabel={'...'}
        pageCount={usersLimit/usersPerPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageClick}
        containerClassName={`pagination ${className}`}
        activeClassName={'pagination_button_active'}
        pageClassName={'pagination_button'}
        breakClassName={'pagination_break'}
      />
    </_Paginate>
  )
}

const _Paginate = styled.div`
  .pagination{
    display: flex;
    align-items: center;
  }

  .pagination_button > a, .pagination_break{
    display: block;
    width: 30px;
    height: 30px;
    border: 1px solid ${Theme.colors.darkBg};
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    margin: 0 5px;
    border-radius: 3px;
  }

  .pagination_button_active{
    a{
      color: ${Theme.colors.accent};
    }
  }
`;

const NextPrevLabel = styled.div`
  height: 30px;
  border: 1px solid ${Theme.colors.darkBg};
  border: 1px solid ${Theme.colors.darkBg};
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border-radius: 3px;
  padding: 0 5px;
  margin: 0 5px;
`;
