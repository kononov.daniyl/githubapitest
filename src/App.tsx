import React from 'react';
import { useRoutes } from './Router';

export const App = () => {
  const routes = useRoutes();

  return (
    <div>
       {routes}
    </div>
  )
}