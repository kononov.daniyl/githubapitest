const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  watch: true,
  devtool: 'inline-sourcemap',
  entry: "./src/index.tsx",
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3000,
    hot: true,
    historyApiFallback: true
  },
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js",
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      src: path.resolve( __dirname, 'src' ),
      '@Theme': path.resolve( __dirname, 'src', 'Theme.ts' ),
      '@Selectors': path.resolve( __dirname, 'src', 'Selectors.ts' ),
      '@Utils': path.resolve( __dirname, 'src', 'utils' ),
      '@Pages': path.resolve( __dirname, 'src', 'pages' ),
      '@Components': path.resolve( __dirname, 'src', 'components' ),
      '@Redux': path.resolve( __dirname, 'src', 'redux' ),
    }
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              compact: true,
              cacheDirectory: true,
              presets: [
                '@babel/preset-env',
                '@babel/preset-react',
                '@babel/preset-typescript'
              ],
              plugins: [
                '@babel/plugin-proposal-class-properties'
              ],
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          'svg-react-loader',
        ]
      },
    ],
  },
  plugins:[
    new HtmlWebpackPlugin({
       template: './public/index.html'
    })
 ]
};
